const THEMES = [
  ['#ebedf0', '#c6e48b', '#7bc96f', '#239a3b', '#196127'],
  ['#ebedf0', '#addffd', '#56b4f5', '#257cf2', '#073777'],
  ['#ebedf0', '#f5a19e', '#ec625d', '#e7362f', '#561315'],
  ['#ebedf0', '#fae46b', '#f9b652', '#dd8808', '#935b05'],
  ['#ebedf0', '#d7b7ff', '#b070ff', '#8624ff', '#600080']
]

export default THEMES
