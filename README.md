
<div align="center">
  <img src="https://user-images.githubusercontent.com/26512984/56975143-240e3000-6bab-11e9-981c-3c3afedc5b20.png">

  Simple github commit history widget for `PC`

  <img src="https://user-images.githubusercontent.com/26512984/57062702-e66cec80-6cfb-11e9-8938-64b12c617bf7.png">

</div>

## How to use

1. Just typing your **Github ID** and Press `ENTER` or click `+` button

<img src="https://user-images.githubusercontent.com/26512984/57066342-e6beb500-6d06-11e9-82ec-5ec2c18b1717.png">

<img src="https://user-images.githubusercontent.com/26512984/57066624-a4e23e80-6d07-11e9-86e4-27ca6c317c94.png">


2. Done! so easy 😃

<img src="https://user-images.githubusercontent.com/26512984/57062702-e66cec80-6cfb-11e9-8938-64b12c617bf7.png">

## Features

- 🏳️‍🌈 Change color theme
  - You can change color theme
(Shortcut: `Ctrl + T`)
<img src="https://user-images.githubusercontent.com/26512984/57066744-03a7b800-6d08-11e9-8e89-f996cecaeaeb.png">


- 🆔 Change user ID
  - Press `ESCAPE` key
  
## Development
[Electron](https://electronjs.org) with [Vue.js](https://vuejs.org)!

#### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build


# lint all JS/Vue component files in `src/`
npm run lint

```

---
